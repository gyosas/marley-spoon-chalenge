namespace :recipes do
  desc 'Fetch recipes_service from Contentful'
  task fetch_contentful: :environment do
    Recipe.fetch_all_from_contentful
    puts 'Recipes fetched from Contentful!'
  end
end
