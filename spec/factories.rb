# frozen_string_literal: true

FactoryBot.define do
  factory :recipe do
    title { 'This is a recipe test' }
    description { 'This is a recipe description test' }
    calories { '700' }
  end
end
