require 'rails_helper'

RSpec.describe RecipesService::Index do
  describe '.call' do
    let(:recipes) { [double('Recipe')] }

    before do
      allow(Recipe).to receive(:all).and_return(recipes)
      allow(RecipesSerializer).to receive(:collection).with(recipes).and_return(recipes)
    end

    it 'returns the serialized recipes collection' do
      expect(described_class.call).to eq(recipes)
    end

    it 'calls Recipe.all to fetch recipes' do
      expect(Recipe).to receive(:all).and_return(recipes)
      described_class.call
    end

    it 'calls RecipesSerializer.collection with recipes' do
      expect(RecipesSerializer).to receive(:collection).with(recipes).and_return(recipes)
      described_class.call
    end
  end
end

