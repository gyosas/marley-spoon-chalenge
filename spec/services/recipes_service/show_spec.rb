require 'rails_helper'

RSpec.describe RecipesService::Show do
  describe '.call' do
    let(:recipe_id) { 1 }
    let(:recipe) { double('Recipe') }

    before do
      allow(Recipe).to receive(:find_by).with(id: recipe_id).and_return(recipe)
      allow(RecipesSerializer).to receive(:record).with(recipe).and_return(recipe)
    end

    it 'returns the serialized recipe record' do
      expect(described_class.call(recipe_id)).to eq(recipe)
    end

    it 'calls Recipe.find_by with the recipe_id' do
      expect(Recipe).to receive(:find_by).with(id: recipe_id).and_return(recipe)
      described_class.call(recipe_id)
    end

    it 'calls RecipesSerializer.record with the recipe' do
      expect(RecipesSerializer).to receive(:record).with(recipe).and_return(recipe)
      described_class.call(recipe_id)
    end
  end
end
