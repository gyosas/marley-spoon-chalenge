# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /recipes' do
  let(:endpoint) { "/recipes" }
  let(:recipe) { FactoryBot.create(:recipe) }
  let(:another_recipe) { FactoryBot.create(:recipe, title: 'Another test recipe title', description: 'Another',
                                           calories: '710') }

  before do
    recipe
    another_recipe
  end

  context "GET /recipes" do
    it "assigns recipes to instance variable" do
      allow(RecipesService::Index).to receive(:call).and_return(RecipesSerializer.collection([recipe, another_recipe]))
      get endpoint

      expect(assigns(:recipes)).to eq(RecipesSerializer.collection([recipe, another_recipe]))
    end

    it "renders the index template" do
      get endpoint
      expect(response).to render_template(:index)
    end
  end
end
