# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /recipes/:id' do
  let(:endpoint) { "/recipes/#{recipe.id}" }
  let(:recipe) { FactoryBot.create(:recipe) }

  before do
    recipe
  end

  context "GET /recipes/id" do
    it "assigns recipes to instance variable" do
      allow(RecipesService::Show).to receive(:call).and_return(RecipesSerializer.record(recipe))
      get endpoint

      expect(assigns(:recipe)).to eq(RecipesSerializer.record(recipe))
    end

    it "renders the show template" do
      get endpoint

      expect(response).to render_template(:show)
    end
  end
end
