# Marley Spoon Chalenge



## Getting started

To make it easy for you to get started with this project, here's a list of recommended next steps.

1. Git clone the project.
2. Run 'bundle install'.
3. Run 'rails db:create db:migrate'.
4. Run 'rake recipes:fetch_contentful'.
5. Run 'rails server' and open the browser and access to /recipes.
