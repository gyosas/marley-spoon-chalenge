class CreateRecipes < ActiveRecord::Migration[7.0]
  def change
    create_table :recipes do |t|
      t.string :title
      t.string :image_url
      t.string :tags
      t.text :description
      t.string :calories
      t.timestamps
    end
  end
end
