# frozen_string_literal: true

class RecipesSerializer
  class << self
    # @param record [Recipe]
    # @return [Hash]
    def record(record)
      record.as_json(except: %i[tags created_at updated_at])
    end

    # @param records [Recipe]
    # @return [Array<Hash>]
    def collection(records)
      records.map { |ar| record(ar) }
    end
  end
end
