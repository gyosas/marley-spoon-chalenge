# frozen_string_literal: true

# Class that handles the index request of RecipesController
class RecipesService::Index
  def self.call
    new.call
  end

  def initialize
  end

  def call
    recipes
  end

  private

  # Grabs all the existing recipes
  # @return [Array<Hash]
  def recipes
    RecipesSerializer.collection(Recipe.all)
  end
end
