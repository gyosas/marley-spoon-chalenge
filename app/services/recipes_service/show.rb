# frozen_string_literal: true

# Class that handles the show request of RecipesController
class RecipesService::Show
  def self.call(recipe_id)
    new(recipe_id).call
  end

  def initialize(recipe_id)
    @recipe_id = recipe_id
  end

  def call
    recipe
  end

  private

  # Filter the Recipe for the given recipe_id
  # @return [Hash]
  def recipe
    RecipesSerializer.record(Recipe.find_by(id: @recipe_id))
  end
end
