class Recipe < ApplicationRecord
  def self.fetch_all_from_contentful
    client = Contentful::Client.new(
      space: 'kk2bw5ojx476',
      access_token: '7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c'
    )

    client.entries(content_type: 'recipe').each do |entry|
      create_or_update_from_contentful(entry)
    end
  end

  def self.create_or_update_from_contentful(entry)
    recipe = find_or_initialize_by(id: entry.id)
    recipe.title = entry.title
    recipe.image_url = 'https:' + entry.photo.url
    recipe.description = entry.description
    recipe.calories = entry.calories
    recipe.save
  end
end
