class RecipesController < ApplicationController
  def index
    @recipes = RecipesService::Index.call
  end

  def show
    @recipe = RecipesService::Show.call(params.require(:id))
  end
end
